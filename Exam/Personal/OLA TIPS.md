### Remember these things. 
<p style ="margin-top: -20px">They are based on feedback from everyone in class in embedded.</p>

- Table of contents
- Conclusion and introduction
- Code in gitlab
- Code in appendix, not the full report. It's fine to keep it in the gitlab repo though.
  - And good desciption of it, where it's used and so on
  - Clean up the code as much as possible
  - And make a diagram of the code, not just include everything w/ descriptions in appendix 
(but remember to do that though).
- It is nice to include a demonstration
  - Make sure to prove everything is working, and we're not "just cheating".
  - Include all the things used in demonstration:
    - Example: RPI, Xbee CO and Xbee EP, Breadboard w/LEDS and ADC's
    - Wireing is also importent (again, diagrams).
- Sometimes, often, it is okay to work in smaller (2-3) groups. Check with the lecturers however.
- It is better to hand in something, even if it is absolute sheite. Because then we can take a discussion
about wether or not we <i>know</i> what we should. The the lecturers can approve it, despite it lacking
a lot (potentially) of things.
- It is <b>very good</b> to have a <b>video</b> on yt (or elsewhere), having a companion site, a physical product
in examns, the grade could be pushed from a 02 to a 10-12 easily.
- In the OLA21 embedded Xbee project, "radio chatter" is bad. Instead of going "LED IS ON", go with 1's and 0's.
This is called <i>Radio Chatter</i>.