### 2nd Semester Project report feedback
*07/06/2021*

If we receive a "minimum requirements" list, make sure you complete it all, however, do not *only* do the minimum things.
Also add extra chapters, if possible. Or, at least rewrite what the requirements are.

When we make a block diagram, use the universally dedicated (shared language for) blocks. Not some icons/emblems of the subjects.

If we make something as electronics, voltage divider for eexample, make sure to mention everything used, and how it is used. And repoduced.
Like resistors, the voltage, the layouts, and so on.

Nikolaj has a report template he recommends using. The formatting for the hand-in should be:
*Final report subject - report title - author name(s).*
I also recommend using this.

Remember remember to put figures on pictures.

We are allowed to make our own custom diagrams. It can look like something but does not **have to** be a specific type. We don't even need to name it.
But really really make sure we know what we are doing in case we use a specific type of diagram. We should be able to descripe it, and explain it.

Everything needs to make the reader able to be able to recreate everything.

For the sake of looking up stuff again in the report, it is *recommended* to make chapers listed, but not *necessary*. Lists in general are good too.

We can divide up inventory more if we please to. 
We don't need to mention what software we have been using in our inventory, but it would be nice to have it mentioned in the report itself.
Or we could also add a section about "development tools".

*"If the report if boring, it is a sign that you are doing it right. Keep it monotone."*