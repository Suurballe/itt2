from digi.xbee.devices import XBeeDevice
from digi.xbee.models.status import NetworkDiscoveryStatus #to send back
from xbee_ledON import turnOn, setup
from xbee_ledOFF import turnOff

#XBEE RECEIVER / ENDPOINT

PORT = "/dev/ttyS0"
BAUD_RATE = 9600
REMOTE_NODE_ID = "CO"
device = XBeeDevice(PORT, BAUD_RATE)

def data_receive_callback(xbee_message):
    print(xbee_message.data.decode())
    xbee_network = device.get_network()
    remote_device = xbee_network.discover_device(REMOTE_NODE_ID)
    if xbee_message.data.decode() == "TURN ON":
        turnOn()
        device.send_data_async(remote_device, "1") #1 = ON
    elif xbee_message.data.decode() == "TURN OFF":
        turnOff
        device.send_data_async(remote_device, "2") #2 = OFF

def main():

    try:
        device.open()
        device.add_data_received_callback(data_receive_callback)

        print("Waiting for data...\n")
        input()

    finally:
        if device is not None and evice.is_open():
            device.close()

if __name__ == '__main__':
    setup()    #runs GPIO setup from libs
    main()



