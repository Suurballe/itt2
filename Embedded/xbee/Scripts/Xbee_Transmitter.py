import time
from digi.xbee.models.status import NetworkDiscoveryStatus #IT IS IMPORTANT TO RUN IN 'XBEE' VIRTUAL ENVIORNMENT!
from digi.xbee.devices import XBeeDevice 

#XBEE TRANSMITTER / COORDINATOR

PORT = "COM5"           #check if this is right, see XCTU if in doubt
BAUD_RATE = 9600
REMOTE_NODE_ID = "END"  #check EP name, sends to

def main():
    local_device = XBeeDevice(PORT, BAUD_RATE)

    try:
        local_device.open()
        xbee_network = local_device.get_network()
        xbee_network.set_discovery_timeout(10)  #in seconds
        xbee_network.clear()
        xbee_network.add_device_discovered_callback(callback_device_discovered)
        xbee_network.add_discovery_process_finished_callback(callback_discovery_finished)
        xbee_network.start_discovery_process()

        print(">Looking for Xbee devices...")

        while xbee_network.is_discovery_running():
            time.sleep(0.5)
        remote_device = xbee_network.discover_device(REMOTE_NODE_ID)

        if remote_device is None:
            print(">No remote device found. Exiting...")
            exit()

        while True:
            DATA_TO_SEND = input(">What would you like to send?")
            print("Message from "+REMOTE_NODE_ID+": ")
            local_device.send_data(remote_device, DATA_TO_SEND)
            local_device.add_data_received_callback(data_receive_callback)
            time.sleep(0.1)

    finally:
        if local_device is not None and local_device.is_open():
            local_device.close()

def callback_device_discovered(remote):
    print(">Device found: %s" % remote)

def callback_discovery_finished(status):
    if status == NetworkDiscoveryStatus.SUCCESS:
        print(">Discovery finished.")
    else:
        print(">Error: %s" % status.description)

def data_receive_callback(xbee_message):
            print(xbee_message.data.decode())

if __name__ == '__main__':
    main()