import RPi.GPIO as GPIO
from digi.xbee.devices import XBeeDevice

PORT = "/dev/ttyS0"
BAUD_RATE = 9600
led = 37    #GPIO.26
GPIO.setwarnings(False)    #shhh
REMOTE_NODE_ID = "C0"
local_device = XBeeDevice(PORT, BAUD_RATE)

def data_receive_callback(xbee_message):
    print(xbee_message.data.decode())
    xbee_network = local_device.get_network()
    remote_device = xbee_network.discover_device(REMOTE_NODE_ID)
    if xbee_message.data.decode() == "LED ON":
        GPIO.output(led, GPIO.HIGH)
        local_device.send_data_async(remote_device, "LED has been turned on")
    elif xbee_message.data.decode() == "LED OFF":
        GPIO.output(led, GPIO.LOW)
        local_device.send_data_async(remote_device, "LED has been turned off")
    else:
        print("Unrecognized input")
        local_device.send_data_async(remote_device, "unrecognized input")

def setup():
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(led, GPIO.OUT)
    GPIO.output(led, GPIO.LOW)

def main():

    try:
        local_device.open()
        local_device.add_data_received_callback(data_receive_callback)

        print("Waiting for data...\n")
        input()

    finally:
        if local_device is not None and local_device.is_open():
            local_device.close()

def destroy():
    GPIO.output(pin, GPIO.LOW)
    GPIO.cleanup()    #CTRL+C
    exit()

if __name__ == '__main__':
    setup()
    try:
        main()
    except KeyboardInterrupt:
        destroy()



