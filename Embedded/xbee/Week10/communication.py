import time
from digi.xbee.models.status import	NetworkDiscoveryStatus
from digi.xbee.devices import XBeeDevice
from digi.xbee.devices import XBee64BitAddress

PORT = "COM5"
BAUD_RATE = 9600
REMOTE_NODE_ID = "END"

class communication():	#our main class


	def __init__(self, uart_device):
		self.local_uart = uart_device


	def interface(self):
		#take inputs and returns ID & MAC
		#takes message and sends
		#returns message received
		pass


	def discovery():		#OUR PART STARTS HERE
		local_device = XBeeDevice(PORT, BAUD_RATE)

		try:
			local_device.open()
			xbee_network = local_device.get_network()
			xbee_network.set_discovery_timeout(15)
			xbee_network.clear()

			xbee_network.add_device_discovered_callback(callback_device_discovered)
			xbee_network.add_discovery_process_finished_callback(callback_discovery_finished)
			xbee_network.start_discovery_process()

			print("searching for remote Xbee devices")

			while xbee_network.is_discovery_running():
				time.sleep(0.5)
			remote_device = xbee_network.discover_device(REMOTE_NODE_ID)
			if remote_device is None:
				print("Could not find the remote device")
				exit(1)

			DATA_TO_SEND = input("What do you want the RPI to do? ex. LED ON/LED OFF")
			print("Sending data to %s >> %s..." % (remote_device.get_64bit_addr(), DATA_TO_SEND))
			local_device.send_data(remote_device, DATA_TO_SEND)
			print("Success")
			local_device.add_data_received_callback(data_receive_callback)


		finally:
			if local_device is not None and local_device.is_open():
				local_device.close()


	def callback_device_discovered(remote):
		print("We found this device: %s" % remote)


	def callback_discovery_finished(status):
		if status == NetworkDiscoveryStatus.SUCCESS:
			print("0 problems detected during discovery")
		else:
			print("Something went wrong: %s" % status.description)


	def data_receive_callback(xbee_message):
				print(xbee_message.data.decode())

	#OUR PART ENDS HERE

if __name__ == '__main__':
	communication.discovery() #?