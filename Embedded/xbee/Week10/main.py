from coordinator import controller # import the controller class from our coordinator.py script in root

def main():

        starter = controller()
        del starter #removes it from memory after we've used it

if __name__ == "__main__":
    main()