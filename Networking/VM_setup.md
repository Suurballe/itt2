### VMware VM Setup
**This guide is a step by step for setting up and connecting VMs in VMware Workstation.**

*VMs*
1) Install VMs on the station. Use ISO files, and *remember* to name the VM appropriately.
2) For the sake of Networking assignments, name the machines PC1 to PC4. **However**, make the changes first, then clone the machines after.
3) For the sake of ease, change the layout of the keyboard (settings --> keyboard --> layout)
4) Make sure the Network Adapter is set to Custom(VMnetX), which has a NAT connection and DHCP connection.
5) Open the terminal and run *sudo apt update*, and then *sudo apt upgrade*.
6) Install the current softwares:
   - sudo apt install wireshark-qt
   - sudo apt-get install open-vm-tools-desktop
   - sudo apt install net-tools
7) Clone the machine to PC2, PC3 and PC4.
8) Customize the machines to make it easier to traverse (sudo nano /etc/hostname) (background image).
___
*vSRXs*

1) There should be a online clone of a functional vSRX on Google Drive.
2) There should be two folders. Move them to your VM folder (most likely 'documents').
3) In each folder, open the vSRX.vmx file with VMware, and 'Take Ownership'.
4) Power it on, and say "I Copied It".
5) Log in with "root" and your generic password.
    - To change password:
    - Type *cli* and then *edit*,
    - Type set system root-authentication plain-text-password
    - *commit*.
___
*Network setup*
1) Your Virtual Network Editor should be as following:
   - VMnet0 = NAT, DHCP Enabled (*as the only one*), 192.168.9.0.
   - VMnet1 = Custom, 192.168.10.0
   - VMnet2 = Custom, 192.168.11.0
   - VMnet3 = Custom, 192.168.12.0
   - VMnet4 = Custom, 192.168.13.0
2) PC1 should be connected to VMnet1, PC2 to VMnet2, and so on.
3) Set up a static IP address, open the network settings on your VM, go to IPv4, set method to manual and:
    - Set the IP for PC1 to 192.168.10.2 and Gateway to .1
    - PC2 to 192.168.11.2 and Gateway to .1
    - PC3 to 192.168.12.2 and Gateway to .1
    - PC4 to 192.168.13.2 and Gateway to .1
    - The Netmask for all is 24
4) Restart.
5) Remember to start your vSRX(s). Note: Not all PC's can ping each other because of the vSRX network.