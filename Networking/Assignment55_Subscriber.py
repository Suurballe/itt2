import paho.mqtt.client as mqtt # import mqtt client

def on_connect(client, userdata, flags, rc):
    print("Connected with the result code "+str(rc))
    client.subscribe("ass55")

def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("192.168.1.12")

client.loop_forever()