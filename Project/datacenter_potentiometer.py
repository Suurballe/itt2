import time
from ADCDevice import *
import paho.mqtt.client as mqtt

adc = ADS7830()    #defines adc type
address="hobelab-laptop.duckdns.org"   #or mqtt.eclipseprojects.io for test. hobelab-laptop.duckdns.org otherwise
client = mqtt.Client("P1")   #creates new instance P1
client.username_pw_set("TeamB2","mcp3202")   #sets username and password for entry
client.connect(address)
topic = "MVP_Test_B2"
        
def loop():
    while True:
        value = adc.analogRead(0)
        voltage = value / 255.0 * 3.3    #calculates voltage in 8-bit (255 resolution) and converts to 3.3.
        print ("Voltage value: %.2f"%(voltage))    #print for confirmation
        client.publish(topic, round(voltage, 3))   #rounding up to 3 decimals
        time.sleep(0.1)
        
def destroy():
    adc.close()
    print("Stopping...")
    exit(0)
    
if __name__ == '__main__':
    print ("Program is booting, starting")
    try:
        loop()
    except KeyboardInterrupt:    # CTRL+C to end
        destroy()